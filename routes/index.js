const express = require('express');
const router = express.Router();
const keycloak = require('./Keycloak');

router.use(keycloak);

module.exports = router;