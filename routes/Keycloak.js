const express = require('express');
const KeycloakController = require('./../controllers/KeycloakController');

const router = express.Router();
router.post('/login', KeycloakController.login);
router.get('/validateAccessToken', KeycloakController.validateAccessToken);
module.exports = router;