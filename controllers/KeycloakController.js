const request = require('request');

login = async (req, res) => {
  const body = req.body;
  const { username = '', password = '' } = body;
  const url = `${process.env.KEYCLOAK_BASEURL}/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/token`;
  await request.post(url, {
    form: {
      username,
      password,
      client_id: process.env.KEYCLOAK_CLIENT_ID,
      grant_type: process.env.KEYCLOAK_GRANT_TYPE
    }
  }, function (error, response, body) {
    if (error) {
      console.log(error);
      return res.status(404).json({ success: false, error: 'Login Failed' });
    }
    return res.status(200).json({ success: true, authentication: body });
  });
}

validateAccessToken = async (req, res) => {
  const accessToken = req.query.accessToken;
  const url = `${process.env.KEYCLOAK_BASEURL}/${process.env.KEYCLOAK_REALM}/protocol/openid-connect/userinfo`;
  await request.get({
    url,
    headers: {
      'Authorization': `Bearer ${accessToken}`
    }
  }, function (err, response, body) {
    body = JSON.parse(body);
    const { error = '' } = body;
    if (!error) {
      return res.status(200).json({ success: true, message: 'User is login' });
    }
    return res.status(404).json({ success: false, message: 'User is logout' });
  });
}

module.exports = {
  login,
  validateAccessToken
};